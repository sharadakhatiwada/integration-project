<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// use App\Mail\WelcomeMail;
// use Illuminate\Support\Facades\Mail;
// Route::get('/email', function () {
//     Mail::to('email@email.com')->send(new WelcomeMail());
//     return new WelcomeMail();
// });


Route::get('/', 'App\Http\Controllers\HomeController@index')->name('base');







Route::get('/home', 'App\Http\Controllers\HomeController@index')->name('home');
Route::get('/blogs/{category_id}', 'App\Http\Controllers\PostController@viewBlogs')->name('view.blogs');

Route::get('/blogs','App\Http\Controllers\PostController@blogs')->name('blog.view');
Route::get('/about','App\Http\Controllers\PostController@about')->name('about.view');
Route::get('/singleBlog/{post_id}','App\Http\Controllers\PostController@singleBlog')->name('singleBlog.view');
Route::get('/destinations','App\Http\Controllers\PostController@destinations')->name('travelDestination.view');
Route::get('/elements','App\Http\Controllers\PostController@elements')->name('element.view');
Route::get('/contacts','App\Http\Controllers\PostController@contacts')->name('contact.view');
Route::get('/destinationDetails','App\Http\Controllers\PostController@destinationDetails')->name('destination.view');





// Route::any('posts.index',function(){
//     $q = Input::get ( 'q' );
//     $user = User::where('name','LIKE','%'.$q.'%')->orWhere('email','LIKE','%'.$q.'%')->get();
//     if(count($user) > 0)
//         return view('welcome')->withDetails($user)->withQuery ( $q );
//     else return view ('welcome')->withMessage('No Details found. Try to search again !');
// });

Route::post('/searchBlogs','App\Http\Controllers\PostController@searchBlogs')->name('blogs.search');



Auth::routes();

    Route::get('/admin', 'App\Http\Controllers\AdminController@admin')    
        ->middleware('is_admin')    
        ->name('admin');



Route::resource('categories','App\Http\Controllers\CategoryController') ->middleware('is_admin');
Route::resource('posts','App\Http\Controllers\PostController') ->middleware('is_admin');
Route::resource('tags','App\Http\Controllers\TagController') ->middleware('is_admin');
Route::resource('settings','App\Http\Controllers\SettingController') ->middleware('is_admin');
Route::resource('settings','App\Http\Controllers\SettingController') ->middleware('is_admin');
Route::resource('social_medias','App\Http\Controllers\SocialMediaController') ->middleware('is_admin');
Route::resource('testimonials','App\Http\Controllers\TestimonialController') ->middleware('is_admin');


Route::resource('subscribes','App\Http\Controllers\SubscribeController');







