

@extends('home')
@section('homecontent')

<div class="row">
    <div class="col-sm-12">    
        <h1 class="display-4">Setting</h1>   
        <a href="{{ route('settings.create') }}" class="btn btn-primary">Add</a>  
        <table class="table table-striped">    
            <thead>        
            <tr>          
                <!-- <td> Category ID</td>           -->
                <td>Type</td>
                <td>Title</td>
               
                <td>Route</td>
                <td>Parent ID</td>
               

                <td colspan = 2>Actions</td>        
            </tr>    
            </thead>    
            <tbody>        
                @foreach($settings as $setting)        
                    <tr>            
                       
                        <td>{{$setting->type}}</td>
                        <td>{{$setting->title}} </td>
                       
                        <td>{{$setting->route}} </td>
                        <td>{{$setting->parent_id}}</td>
                        
                       
                        
                        
                        <td>                                      
                            <a href="{{ route('settings.edit',$setting->id)}}" class="btn btn-primary">Edit Setting</a>
                                                                  

                        </td> 
                        
                        
                        
                        <td>
                          <form action="{{ route('settings.destroy',$setting->id)}}" method="post">                  
                                @csrf                  
                               @method('DELETE')                  
                                <button class="btn btn-danger" type="submit">Delete</button>
                            </form>    
                                      
                        </td> 
                        
          
                        
                    </tr>  




           





                @endforeach    
            </tbody>  
        </table>
    <div>
</div>@endsection

