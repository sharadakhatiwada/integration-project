@extends('home')
@section('homecontent')
<div class="row"> 
    <div class="col-sm-8 offset-sm-2">    
        <h3 class="display-4">Add Setting</h3>  
        <div>    
            @if ($errors->any())      
            <div class="alert alert-danger">        
                <ul>            
                @foreach ($errors->all() as $error)              
                <li>{{ $error }}</li>            
                @endforeach        
                </ul>      
            </div>
            <br />    
            @endif      
            <form enctype="multipart/form-data" method="post" action="{{ route('settings.store') }}">          
            @csrf 
            <div class="form-group">
                <label for="type">Select Type</label>
               
                <select name="type" class="form-control" >
               
                <option value="header">header</option>
              
                </select>
                </div>
            
             
            <div class="form-group d-flex flex-column">                  
                <label for="title">Title:</label>          
                <input type="text"  name="title"/>  
                       
            </div> 
            
            
            <!-- <div class="form-group d-flex flex-column">                  
                <label for="route">Route:</label>          
                <input type="text"  name="route"/>  
                       
            </div> -->
            <div class="form-group">
                <label for="parent_id">Select Route</label>
               
                <select name="route" class="form-control">
                
                <option value="home">home</option>
                <option value="about.view">about</option>
                <option value="travelDestination.view">travel destination</option>
                <option value="destination.view">destination</option>
                <option value="element.view">element</option>
                <option value="blog.view">blog</option>
                <option value="contact.view">contact</option>
               
                
                
                
                </select>
            </div>
           
            <div class="form-group">
                <label for="parent_id">Select Parent</label>
               
                <select name="parent_id" class="form-control" id="parent_id">
               <option value="">select</option>
                @foreach($settings as $setting)
                <option value="{{$setting->id}}">{{$setting->title}}</option>
               
                @endforeach
                
                
                </select>
            </div>
                  
                           
            <button type="submit" class="btn btn-success">Save</button>
            <a href="{{ route('settings.index') }}" class="btn btn-primary">Cancel</a>  
                
            </form>  
        </div>
    </div>
</div>
@endsection