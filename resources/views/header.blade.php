
<header>
        <div class="header-area ">
            <div id="sticky-header" class="main-header-area">
                <div class="container-fluid">
                    <div class="header_bottom_border">
                        <div class="row align-items-center">
                            <div class="col-xl-2 col-lg-2">
                                <div class="logo">
                                    <a href="{{route('base')}}">
                                        <img src="img/logo.png" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="col-xl-6 col-lg-6">
                                <div class="main-menu  d-none d-lg-block">
                                    <nav>
									<ul id="navigation">


                                        @foreach ($headers as $header)
                                            <li><a class="active" href="{{$header->route==null?'#':route($header->route)}}">{{$header->title}}
                                            <!-- @if(count($header->childs))
                                            
                                            <i class="ti-angle-down">
                                        @endif -->
                                            
                                            </a>
                                           
                                            @if(count($header->childs))

                                                @include('sub_header',['childs' => $header->childs])
                                            @endif
                                            </li>
                                        @endforeach



                                            <!-- <li><a class="active" href="{{route('home')}}">home</a></li> -->
                                            <!-- <li><a href="{{route('about.view')}}">About</a></li> -->
                                            <!-- <li><a class="" href="{{route('travelDestination.view')}}">Destination</a></l/li>
                                            <li><a href="#">pages <i class="ti-angle-down"></i></a>
                                                <ul class="submenu">
                                                        <li><a href="{{route('destination.view')}}">Destinations details</a></li>
                                                        <li><a href="{{route('element.view')}}">elements</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="{{route('blog.view')}}">blog</a>
                                              
                                            </li>
                                            <li><a href="{{route('contact.view')}}">Contact</a></li> -->
                                            <li><a href="#">Auth <i class="ti-angle-down"></i></a>
                                            <ul class="submenu">
                                                    <!-- Authentication Links -->
                                                    @guest
                                                        <li>
                                                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                                        </li>
                                                        @if (Route::has('register'))
                                                            <li>
                                                                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                                            </li>
                                                            
                                                        @endif
                                                    @else
                                                        <li>
                                                            <a id="navbarDropdown"  href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                                                {{ Auth::user()->name }}
                                                            </a>
                                                            <a href="{{route('categories.index')}}">Admin</a>
                                                            <div  aria-labelledby="navbarDropdown">
                                                                <a href="{{ route('logout') }}"
                                                                onclick="event.preventDefault();
                                                                                document.getElementById('logout-form').submit();">
                                                                    {{ __('Logout') }}
                                                                </a>

                                                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                                                    @csrf
                                                                </form>
                                                                    
                                                                </div>
                                                                
                                                                    </li>
                                                                @endguest
                                                            </ul>
                                                                    
                                                                                    
                                                </li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-4 d-none d-lg-block">
                                <div class="social_wrap d-flex align-items-center justify-content-end">
                                    <div class="number">
                                        <p> <i class="fa fa-phone"></i> 10(256)-928 256</p>
                                    </div>
                                    <div class="social_links d-none d-xl-block">
                                   
                                        <ul>
                                        @foreach($social_medias as $social_media)
                                            <li><a href="{{url($social_media->link)}}" ><i class="{{$social_media->icon}}"></i> </a></li>
                                            @endforeach
                                      
                                        </ul>
                                     
                                    </div>
                                </div>
                            </div>
                            <div class="seach_icon">
                                <a data-toggle="modal" data-target="#exampleModalCenter" href="#">
                                    <i class="fa fa-search"></i>
                                </a>
                            </div>
                            <div class="col-12">
                                <div class="mobile_menu d-block d-lg-none"></div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </header>
    <!-- header-end -->
    