@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
        <a href="{{ route('posts.index') }}">Post</a>
        <a href="{{ route('categories.index') }}">Category</a>
        <a href="{{ route('tags.index') }}">Tag</a>
        <a href="{{ route('settings.index') }}">Setting</a>
        <a href="{{ route('social_medias.index') }}">Social Media</a>
        <a href="{{ route('testimonials.index') }}">Testimonial</a>
        </div>  

      
    </div>
    <div class="col-10">
            @yield('homecontent')
        </div>

        
@endsection
 