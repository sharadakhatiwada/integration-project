@extends('home')
@section('homecontent')
<div class="row">    
    <div class="col-sm-8 offset-sm-2">        
        <h3 class="display-4">Update Category</h3>        
        @if ($errors->any())        
        <div class="alert alert-danger">            
            <ul>                
            @foreach ($errors->all() as $error)                
            <li>{{ $error }}</li>                
            @endforeach            
            </ul>        
        </div>        
        <br />         
        @endif        
        <form enctype="multipart/form-data" method="post" action="{{ route('categories.update',$category->id) }}">            
        @method('PATCH')             
        @csrf  

                  
            <div class="form-group">                
                <label for="name">Name:</label>                
                <input type="text" class="form-control" name="name" value={{ $category->name }} />            
            </div>  
            <div class="form-group d-flex flex-column">                
                <label for="image">Image:</label>                
                <input type="file"  name="image" value={{ $category->image_name }}/>  
            </div>           
           

            <button type="submit" class="btn btn-success">Update</button>        
            <a href="{{ route('categories.index') }}" class="btn btn-primary">Cancel</a>                        
        </form>
    </div>
</div>
@endsection