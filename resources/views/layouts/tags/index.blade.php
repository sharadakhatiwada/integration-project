

@extends('home')
@section('homecontent')
<div class="row">
    <div class="col-sm-12">    
        <h1 class="display-4">Tag</h1>   
        <a href="{{ route('tags.create') }}" class="btn btn-primary">Add Tag</a> 
       
        <table class="table table-striped">    
            <thead>        
            <tr>
            <td>Type</td>            
                
                
                <td colspan = 2>Actions</td>        
            </tr>    
            </thead>    
            <tbody>        
                @foreach($tags as $tag)     

                 
                    <tr>            
                    <td>{{$tag->type}}</td>      
                        
                  
                        
                       
                       
                         <td>                                      
                            <a href="{{ route('tags.edit',$tag->id)}}" class="btn btn-primary">Edit Post</a>  
                                      
                        </td> 
                        <td>
                          <form action="{{ route('tags.destroy',$tag->id)}}" method="post">                  
                                @csrf                  
                                @method('DELETE')                  
                                <button class="btn btn-danger" type="submit">Delete</button>                
                            </form>    
                                      
                        </td>  
                                   
                        
                    </tr>
                    @endforeach  
                  
            </tbody>  
        </table>


       
    <div>
</div>@endsection

