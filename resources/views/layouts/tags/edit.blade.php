@extends('home')
@section('homecontent')
 
<div class="row">    
    <div class="col-sm-8 offset-sm-2">        
        <h3 class="display-4">Update Tag</h3>        
        @if ($errors->any())        
        <div class="alert alert-danger">            
            <ul>                
            @foreach ($errors->all() as $error)                
            <li>{{ $error }}</li>                
            @endforeach            
            </ul>        
        </div>        
        <br />         
        @endif        
        <form method="post" action="{{ route('tags.update',$tag->id) }}"> 
                        
        @method('PUT')             
        @csrf  
        
            
        <div class="form-group">                
                <label for="type">Type:</label>                
                <input type="text" class="form-control" name="type" value={{ $tag->type }} />            
            </div> 
             <button type="submit" class="btn btn-success">Update</button>        
            <a href="{{ route('tags.index') }}" class="btn btn-primary">Cancel</a>                        
        </form>
    </div>
</div>
@endsection