

@extends('home')
@section('homecontent')
<div class="row">
    <div class="col-sm-12">    
        <h1 class="display-4">Social Media</h1>   
        <a href="{{ route('social_medias.create') }}" class="btn btn-primary">Add Social Media</a> 
       
        <table class="table table-striped">    
            <thead>        
            <tr>
            <td>Name</td>   
            <td>Link</td> 
            <td>icon</td>   
                
                
                
                <td colspan = 2>Actions</td>        
            </tr>    
            </thead>    
            <tbody>        
                @foreach($social_medias as $socialmedia)     

                 
                    <tr>            
                    <td>{{$socialmedia->name}}</td>   
                    <td>{{$socialmedia->link}}</td> 
                    <td>{{$socialmedia->icon}}</td>     
                                        
                        
                  
                        
                       
                       
                         <td>                                      
                            <a href="{{ route('social_medias.edit',$socialmedia->id)}}" class="btn btn-primary">Edit Social Media</a>  
                                      
                        </td> 
                        <td>
                          <form action="{{ route('social_medias.destroy',$socialmedia->id)}}" method="post">                  
                                @csrf                  
                                @method('DELETE')                  
                                <button class="btn btn-danger" type="submit">Delete</button>                
                            </form>    
                                      
                        </td>  
                                   
                        
                    </tr>
                    @endforeach  
                  
            </tbody>  
        </table>


       
    <div>
</div>@endsection

