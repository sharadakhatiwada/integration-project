@extends('home')
@section('homecontent')
 
<div class="row">    
    <div class="col-sm-8 offset-sm-2">        
        <h3 class="display-4">Update Social Media</h3>        
        @if ($errors->any())        
        <div class="alert alert-danger">            
            <ul>                
            @foreach ($errors->all() as $error)                
            <li>{{ $error }}</li>                
            @endforeach            
            </ul>        
        </div>        
        <br />         
        @endif        
        <form method="post" action="{{ route('social_medias.update',$social_media->id) }}"> 
                        
        @method('PUT')             
        @csrf  
        
        <div class="form-group">
                <label for="socialMedia_id">Select Social Media</label>
               
                <select name="name" class="form-control">
                
                <option value="facebook">Facebook</option>
                <option value="instagram">Instagram</option>
                <option value="linkedin">Linkedin</option>
                <option value="google">Google</option>
           
                </select>
                </div>  
        <div class="form-group">                
                <label for="link">Link:</label>                
                <input type="text" class="form-control" name="link" value={{ $social_media->link}} />            
            </div> 
            <div class="form-group">
                <label for="socialMedia_id">Select Icon</label>
               
                <select name="icon" class="form-control">
                
                <option value="fa fa-facebook">facebook</option>
                <option value="fa fa-instagram">instagram</i></option>
                <option value="fa fa-linkedin">linkedin</option>
                <option value="fa fa-google">google</option>
           
                </select>
                </div>
             <button type="submit" class="btn btn-success">Update</button>        
            <a href="{{ route('social_medias.index') }}" class="btn btn-primary">Cancel</a>                        
        </form>
    </div>
</div>
@endsection