@extends('home')
@section('homecontent')
<script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
            <script>
            tinymce.init({
                selector: 'textarea',
                plugis:'link code',
                menubar: false
                });
            
            </script> 
<div class="row">    
    <div class="col-sm-8 offset-sm-2">        
        <h3 class="display-4">Update Post</h3>        
        @if ($errors->any())        
        <div class="alert alert-danger">            
            <ul>                
            @foreach ($errors->all() as $error)                
            <li>{{ $error }}</li>                
            @endforeach            
            </ul>        
        </div>        
        <br />         
        @endif        
        <form enctype="multipart/form-data"  method="post" action="{{ route('posts.update', [$post->id, $category->id,$tag->id]) }}"> 
                        
        @method('PUT')             
        @csrf  
        
            <div class="form-group"> 
            <div class="form-group">                
                <label for="title">:</label>                
                <input type="text" class="form-control" name="title" value={{ $post->title }}/>  
            </div> 
        
                           
                        
            <div class="form-group"> 
                            
                            <label for="description">description:</label>              
                            <textarea rows="5" cols="60"  name="description" value={{ $post->description }}> Enter the description here... </textarea>          
                        </div> 
            <div class="form-group d-flex flex-column">                
                <label for="image">Image:</label>                
                <input type="file"  name="image" value={{ $post->image_name }}/>  
            </div>  
            <div class="form-group">
                <label for="category_id">Select Category</label>
               
                <select name="category_id" class="form-control" id="category_id">
                @foreach($categories as $category)
                <option value="{{$category->id}}">{{$category->name}}</option>
               
                @endforeach
                
                </select>
            </div>
            <div class="form-group d-flex flex-column">
               
                <label for="tag_id">Choose Tag:</label>

                    <select name="tag_id" class="form-control" id="tag_id" multiple>
                    @foreach($tags as $tag)
                    <option value="{{$tag->id}}">{{$tag->type}}</option>
            @endforeach
            </select>
            </div>
             <button type="submit" class="btn btn-success">Update</button>        
            <a href="{{ route('posts.index') }}" class="btn btn-primary">Cancel</a>                        
        </form>
    </div>
</div>
@endsection