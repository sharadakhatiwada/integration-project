

@extends('home')
@section('homecontent')
<div class="row">
    <div class="col-sm-12">    
        <h1 class="display-4">Post</h1>   
        <a href="{{ route('posts.create') }}" class="btn btn-primary">Add Post</a> 
        
        <table class="table table-striped">    
            <thead>        
            <tr>
            <td>Title</td>            
                <td>Description</td>          
                <td>Image</td>
                <td> Category</td>
                <td> Tag</td>
                
                <td colspan = 2>Actions</td>        
            </tr>    
            </thead>    
            <tbody>        
                @foreach($posts as $post)     

                 
                    <tr>            
                    <td>{{$post->title}}</td>      
                        <td>{!!$post->description!!}
                        <!-- <button onclick="description()" id="{{$post->id}}">Read more</button> -->
                  
                        
                        </td>
                        <td>{{$post->image_name}}
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#imageModal-{{$post->id}}">
                            View
                            </button></td>
                        <td>{{$post->category !==null?  $post->category->name :''}}</td>
                        <td>{{$post->tags !==null?  $post->tags->map(function ($tag) {
                        return collect($tag)->only(['type']);
                      })->pluck('type')->implode(','):''}}</td>
                        


                     
                        

                       
                         <td>                                      
                            <a href="{{ route('posts.edit',$post->id)}}" class="btn btn-primary">Edit Post</a>  
                                      
                        </td> 
                        <td>
                          <form action="{{ route('posts.destroy',$post->id)}}" method="post">                  
                                @csrf                  
                                @method('DELETE')                  
                                <button class="btn btn-danger" type="submit">Delete</button>                
                            </form>    
                                      
                        </td>  
                                   
                        
                    </tr>
                    <div class="modal fade" id="imageModal-{{$post->id}}" tabindex="-1" role="dialog" aria-labelledby="imageModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Image</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body"> 
      <img src="{{ URL::to('/') }}/images/{{$post->image_name}}" width="100%">  
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>         
                @endforeach    
            </tbody>  
        </table>


       
    <div>
</div>@endsection

