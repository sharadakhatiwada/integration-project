

@extends('home')
@section('homecontent')

<div class="row">
    <div class="col-sm-12">    
        <h1 class="display-4">Testimonial</h1>   
        <a href="{{ route('testimonials.create') }}" class="btn btn-primary">Add</a>  
        <table class="table table-striped">    
            <thead>        
            <tr>          
               
                <td>Description</td>        
                <td>Name</td>
                <td> Image</td>
               

                <td colspan = 2>Actions</td>        
            </tr>    
            </thead>    
            <tbody>        
                @foreach($testimonials as $testimonial)      
                    <tr>            
                     <td>{{$testimonial->description}}</td>          
                        <td>{{$testimonial->name}}</td>
                        <td>{{$testimonial->image_name}}
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#imageModal-{{$testimonial->id}}">
                            View
                            </button>
                            </td>
                        
                        
                        <td>                                      
                            <a href="{{ route('testimonials.edit',$testimonial->id)}}" class="btn btn-primary">Edit Testimonial</a>
                                                                  

                        </td> 
                        
                        
                        
                         
                        <td>
                          <form action="{{ route('testimonials.destroy',$testimonial->id)}}" method="post">                  
                                @csrf                  
                               @method('DELETE')                  
                                <button class="btn btn-danger" type="submit">Delete</button>
                            </form>    
                                      
                        </td>  
                        
          
                        
                    </tr>  



<div class="modal fade" id="imageModal-{{$testimonial->id}}" tabindex="-1" role="dialog" aria-labelledby="imageModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Image</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body"> 
      <img src="{{ URL::to('/') }}/images/{{$testimonial->image_name}}" width="100%">  
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>         





                @endforeach    
            </tbody>  
        </table>
    <div>
</div>@endsection

