@extends('home')
@section('homecontent')



<div class="row"> 
    <div class="col-sm-8 offset-sm-2">    
        <h3 class="display-4">Testimonial</h3>  
        <div>    
            @if ($errors->any())      
            <div class="alert alert-danger">        
                <ul>            
                @foreach ($errors->all() as $error)              
                <li>{{ $error }}</li>            
                @endforeach        
                </ul>      
            </div>
            <br /> 

            
            @endif   

              
            <form enctype="multipart/form-data" method="post" action="{{ route('testimonials.store') }}">
             @csrf 
            
             <div class="form-group">                  
                <label for="description">Description:</label>              
                <input type="text" class="form-control" name="description"/>          
            </div>
           
            <div class="form-group">                  
                <label for="name">Name:</label>              
                <input type="text" class="form-control" name="name"/>          
            </div>
            <div class="form-group">                  
                <label for="image">Image:</label>              
                <input type="file" class="form-control" name="image"/>          
            </div>

            

           
             <button type="submit" class="btn btn-success">Save</button>
            <a href="{{ route('testimonials.index') }}" class="btn btn-primary">Cancel</a> 
               
            </form>  
        </div>
    </div>
</div>
@endsection 
