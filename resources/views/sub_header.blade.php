

<ul class="submenu">
<!-- <i class="ti-angle-down"></i> -->
@foreach($childs as $child)
   <li>
   <a class="active" href="{{route($child->route)}}">{{ $child->title }}</a>

       
   @if(count($child->childs))
            @include('sub_header',['childs'=> $child->childs])
        @endif
   </li>
@endforeach
</ul>