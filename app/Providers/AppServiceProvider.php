<?php

namespace App\Providers;
use App\Models\Setting;
use App\Models\SocialMedia;
use View;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
    schema::defaultStringLength(191);

    View::composer('*', function($header)
        {
            $settings=Setting::where('type','=','header')->where('parent_id','=',null)->get();
            $social_medias=SocialMedia::all();
           
           



            
            $header->with('headers', $settings);
            $header->with('social_medias', $social_medias);
           


        });

       
    }
}
