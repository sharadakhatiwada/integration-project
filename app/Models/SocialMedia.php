<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SocialMedia extends Model
{
   
    public $table = 'social_medias';
   
    use HasFactory;
  
    protected $fillable = [ 
        'name',
        'link',
        'icon'
       
           
    ];
    
}
