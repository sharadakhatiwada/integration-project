<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    use HasFactory;
    public function childs(){
        return $this->hasMany('App\Models\Setting','parent_id','id');
    }


    protected $fillable = [ 
        'type', 
        'title',      
                     
        'route',  
        'parent_id'
           
    ];
}
