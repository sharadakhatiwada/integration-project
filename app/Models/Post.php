<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Post extends Model
{
    use HasFactory;
    use SoftDeletes;

    public function category(){
        return $this->belongsTo('App\Models\Category');
    }
    public function tags(){
        return $this->belongsToMany('App\Models\Tag', 'post_tag');
    }
    protected $fillable = [ 
        'title',       
        'description',              
        'image_name',  
        'image_url',
        'category_id',
        'tag_id'    
    ];
}
