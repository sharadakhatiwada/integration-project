<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tag;

class tagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $tags = Tag::all();
             
        return view('layouts.tags.index', compact('tags'));  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $tags = Tag::all();
             
        return view('layouts.tags.create', compact('tags'));  
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([            
            'type'=>'required',                     
           

           ]);
          

           $tag = new Tag([   
               'type'=> $request->get('type')
             
      
          
                  
          ]);  

                  
        
          $tag->save(); 
      
         
          return redirect()->route('tags.index')->with('success', 'Tag saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $tag = Tag::find($id);
              
       return view('layouts.tags.edit',compact('tag'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([            
            'type'=>'required'                    
          
                     
       ]);   
       $tag = Tag::find($id);      
      

          $tag->type=$request->get('type');
         
              $tag->save();
        return redirect()->route('tags.index')->with('success', 'Tag saved!');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        $tag= Tag::find($id); 
            
        $tag->delete();        
        return redirect()->route('tags.index')->with('success', 'tag Deleted!');
    }
}
