<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Testimonial;
use Illuminate\Support\Facades\Auth;
use file;
use view;



class TestimonialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $testimonials =Testimonial::all();
             
        return view('layouts.testimonials.index', compact('testimonials')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $testimonials = Testimonial::all();
             
        return view('layouts.testimonials.create', compact('testimonials')); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $request->validate([            
                               
           'description'=>'required', 
           'name'=>'required', 
           'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'

           ]);
          
           

           if ($request->hasFile('image')) {
               $image = $request->file('image');
               $image_name= time().'.'.$image->getClientOriginalExtension();
               $destinationPath = public_path('/images');
               $image->move($destinationPath, $image_name);  

           }


           $testimonial = new Testimonial([   
            
            'description' => $request->get('description'), 
            'name'=> $request->get('name'),                       
            'image_name'=>$image_name,
           'image_url'=>$destinationPath 
   
       
               
       ]);  
       $testimonial->save();

       return redirect()->route('testimonials.index')->with('success', 'Testimonial saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $testimonial= Testimonial::find($id);
        return view('layouts.testimonials.edit', compact('testimonial'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $request->validate([            
                               
           'description'=>'required', 
           'name'=>'required',  
           'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
                     
       ]);        
      

      $testimonial=Testimonial::find($id);
      File::delete(public_path("/images/").$testimonial->image_name);  
  
  
       if ($request->hasFile('image')) {
           $image = $request->file('image');
           $image_name = time().'.'.$image->getClientOriginalExtension();
           $destinationPath = public_path('/images');
           $image->move($destinationPath, $image_name);
           
       }    
          
            $testimonial->description=$request->get('description');  
            $testimonial->name=$request->get('name');                  
             $testimonial->image_name=$image_name;
             $testimonial->image_url=$destinationPath;
              $testimonial->save();
        return redirect()->route('testimonials.index')->with('success', 'Testimonial saved!');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $testimonial = Testimonial::find($id); 
            
        $testimonial->delete();        
        return redirect()->route('testimonials.index')->with('success', 'Post Deleted!');
    }
    
}
