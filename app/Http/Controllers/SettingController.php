<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Setting;
use view;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $settings = Setting::all();
             
        return view('settings.index', compact('settings'));  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $settings = Setting::all();
             
        return view('settings.create', compact('settings'));  
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([   
            'type'=>'required',         
            'title'=>'required', 
            'route',
            'parent_id'

           
           

           ]);


           $setting = new Setting([   
            'type'=>$request->get('type'), 
            'title'=> $request->get('title'),
            'route'=>$request->get('route'),
            'parent_id'=>$request->get('parent_id')
                                  
            
       ]);  
       $setting->save();
       return redirect()->route('settings.index')->with('success', 'Setting saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $setting= Setting::find($id);
        return view('settings.edit', compact('setting'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $request->validate([    
            'type'=>'required',        
            'title'=>'required', 
            'route',
            'parent_id'                    
               
       ]); 
       $setting=Setting::find($id);

       $setting->type=$request->get('type');
       $setting->title=$request->get('title');  
      $setting->route=$request->get('route');
      $setting->parent_id=$request->get('parent_id');
       $setting->save();
       return redirect()->route('settings.index')->with('success', 'Setting saved!');  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $setting = Setting::find($id); 
            
        $setting->delete();        
        return redirect()->route('settings.index')->with('success', 'Setting Deleted!');
    }
}
