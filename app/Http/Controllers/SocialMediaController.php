<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use App\Models\SocialMedia;

class SocialMediaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $social_medias=SocialMedia::all();
             
        return view('layouts.socialmedias.index', compact('social_medias')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $social_medias= SocialMedia::all();
             
        return view('layouts.socialmedias.create', compact('social_medias'));  
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([   
            'name'=>'required',         
            'link'=>'required', 
            'icon'=>'required'
            

           
           

           ]);


           $social_media = new SocialMedia([   
            'name'=>$request->get('name'), 
            'link'=> $request->get('link'),
            'icon'=> $request->get('icon'),
           
                                  
            
       ]);  
       $social_media->save();
       return redirect()->route('social_medias.index')->with('success', 'SocialMedia saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $social_media= SocialMedia::find($id);
        return view('layouts.socialmedias.edit', compact('social_media'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $request->validate([    
            'name'=>'required',        
            'link'=>'required', 
              'icon'=>'required'                 
               
       ]); 
       $social_media=SocialMedia::find($id);

       $social_media->name=$request->get('name');
       $social_media->link=$request->get('link');  
       $social_media->icon=$request->get('icon');  
     
       $social_media->save();
       return redirect()->route('social_medias.index')->with('success', 'SocialMedia saved!');  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        $social_media = SocialMedia::find($id); 
            
        $social_media->delete();        
        return redirect()->route('social_medias.index')->with('success', 'SocialMedia Deleted!');
    }
}
