<?php

namespace App\Http\Controllers;
use File;
use Illuminate\Http\Request;

use DB;
use App\Models\Category;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $categories = Category::all();        
        return view('layouts.categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('layouts.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $request->validate([            
                                
            'name'=>'required', 
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
                  
        ]);  
        
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $image_name= time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/images');
            $image->move($destinationPath, $image_name);  

        }




        $category = new Category([            
                                
            'name' => $request->get('name'), 
            'image_name'=>$image_name,
            'image_url'=>$destinationPath 
                         
            ]);        
        $category->save(); 
        

        return redirect('/categories')->with('success', 'Category saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $category = Category::find($id);        
        return view('layouts.categories.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([ 
                   
            'name'=>'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'                     
            
                
        ]);      
        
        $category=Category::find($id);
        File::delete(public_path("/images/").$category->image_name);  
 
    
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $image_name = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/images');
            $image->move($destinationPath, $image_name);


        $category = Category::find($id);        
                  
        $category->name = $request->get('name');  
        $category->image_name=$image_name;
               $category->image_url=$destinationPath;
              
        $category->save();        
        return redirect('/categories')->with('success', 'Category updated!');
    }
}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $category = Category::find($id); 
            
        $category->delete();        
        return redirect()->route('categories.index')->with('success', 'Category Deleted!');
    } 


    // public function categoryName(Request $request){
    //     $text= $request->get('category_name');
               
    //       $categories=Category::where('name', 'like', '%' .$text. '%')->get();
    //     $categories=Category::all();
              
    //       return view('blog', compact('categories')); 
  
  
    //   }

   
}
