<?php

namespace App\Http\Controllers;
use File;
use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\Category;
use App\Models\Tag;



class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $posts = Post::all();
             
        return view('layouts.posts.index', compact('posts'));  

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categories=Category::all();
        $tags=Tag::all();
        return view('layouts.posts.create',compact('categories','tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([            
             'title'=>'required',                     
            'description'=>'required', 
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'

            ]);
           
            

            if ($request->hasFile('image')) {
                $image = $request->file('image');
                $image_name= time().'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('/images');
                $image->move($destinationPath, $image_name);  

            }


            $post = new Post([   
                'title'=> $request->get('title'),
                'description' => $request->get('description'),                        
                'image_name'=>$image_name,
               'image_url'=>$destinationPath 
       
           
                   
           ]);  

                   
           $category_id=$request->get('category_id');
           if($category_id==null){
           $post->save();
           }
           else{
            $category=Category::find($category_id);
            $category->posts()->save($post);
           } 
           $tag_ids=$request->get('tag_id');
          
           $post->tags()->attach($tag_ids);
           
           
      
           
       
          
           return redirect()->route('posts.index')->with('success', 'Post saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $post = Post::find($id);
        $category_id=$post->category_id;
        $category = Category::find($category_id);   
        $categories=Category::all(); 
        
        
        $tag_id=$post->tag_id;
        $tag = Tag::find($tag_id);   
        $tags=Tag::all(); 
       return view('layouts.posts.edit',compact('post','category','categories','tag','tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $request->validate([            
              'title'=>'required',                     
             'description'=>'required', 
             'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
                       
         ]);        
        

        $post=Post::find($id);
     File::delete(public_path("/images/").$post->image_name);  
    
    
         if ($request->hasFile('image')) {
             $image = $request->file('image');
             $image_name = time().'.'.$image->getClientOriginalExtension();
             $destinationPath = public_path('/images');
             $image->move($destinationPath, $image_name);
             
         }    
            $post->title=$request->get('title');
              $post->description=$request->get('description');                    
               $post->image_name=$image_name;
               $post->image_url=$destinationPath;
                $post->save();
          return redirect()->route('posts.index')->with('success', 'Post saved!');
          
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $post = Post::find($id); 
            
        $post->delete();        
        return redirect()->route('posts.index')->with('success', 'Post Deleted!');
    } 
    public function blogs(){

        $posts = Post::all();
        $categories=Category::all();
        $tags=Tag::all(); 
       
             
        return view('blog', compact('posts','categories','tags')); 


    }
    public function about(){

        $posts = Post::all();
             
        return view('about', compact('posts')); 


    }
    public function singleBlog($post_id){
        $post= Post::find($post_id);  
        
        $categories=Category::all();
        $tags=Tag::all();
             
        return view('single-blog', compact('post','categories','tags')); 


    }
    public function destinations(){

        $posts = Post::all();
             
        return view('travel_destination', compact('posts')); 


    }
    public function elements(){

        $posts = Post::all();
             
        return view('elements', compact('posts')); 


    }
    public function contacts(){

        $posts = Post::all();
             
        return view('contact', compact('posts')); 


    }
    public function destinationDetails(){

        $posts = Post::all();
             
        return view('destination_details', compact('posts')); 


    }

    public function searchBlogs(Request $request){
      $text= $request->get('search_text');

     
       

        $posts = Post::where( 'title', 'like', '%' . $text . '%')->get();

             
        $categories=Category::all();
             
        return view('blog', compact('posts','categories')); 


    }
    public function viewBlogs($category_id){
        $category = Category::find($category_id);  
       
        $posts= $category->posts;

        $categories=Category::all();
             
        return view('blog', compact('posts','categories')); 
       
    }
   

    



    
}
